<?php
require_once __DIR__.'/vendor/autoload.php';

/* functions */
function dd($v)
{
    die(var_dump($v));
}

function appendTitle($app, $newTitle)
{
    $title = $app['twig']->getGlobals()['title'];
    array_unshift($title, $newTitle);
    $app['twig']->addGlobal('title', $title);
}

function filterBanners($banners, $id_area)
{
    $banners_area = array();
    foreach ($banners as $b)
    {
        if ($b->id_area == $id_area)
        {
            $banners_area[] = $b;
        }
    }
    return $banners_area;
}

function paginaUrl($item) {
    return "/{$item->slug}-{$item->id}";
}
function noticiaUrl($item) {
    return "/noticias/{$item->slug}-{$item->id}";
}
function galeriaUrl($item) {
    return "/galerias/{$item->slug}-{$item->id}";
}

/* getenv setup */
$dotenv = new Symfony\Component\Dotenv\Dotenv();
$dotenv->load(__DIR__.'/.env');

/* Silex */
$app = new Silex\Application();
$app['debug'] = (getenv('APP_DEBUG') === 'true');

/* Twig */
$app->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => __DIR__.'/views'));

/* Assets */
$app['twig']->addFunction(new \Twig_SimpleFunction('asset',
    function ($asset) {
    $asset =  ltrim($asset,'/');
        $assetPath = __DIR__ . '/public/' . $asset;
        if (file_exists($assetPath)) { $asset .= '?' . filemtime($assetPath); }
        return "/{$asset}";
    }
));

/* Alterar tamanho das imagens e vídeos da Central */
$app['twig']->addFilter('resize', new Twig_Filter_Function(
    function($string, $width, $height = false)
    {
        if (!strstr($string, ".jpg")) { return $string; }
        if ($height) { return str_replace('.jpg', "_{$width}x{$height}.jpg", $string); }
        return str_replace('.jpg', "_{$width}.jpg", $string);
    }
));

/* Limitar letras de uma string */
$app['twig']->addFilter('limitLetters', new Twig_Filter_Function(
    function ($string, $limit, $suffix = '...')
    {
        $string = str_replace(PHP_EOL, ' ', $string);
        return (strlen($string) > $limit)? explode(PHP_EOL,wordwrap($string, $limit, PHP_EOL))[0] . $suffix : $string;
    }
));

$app['twig']->addFilter('var_dump', new Twig_Filter_Function('var_dump'));
$app['twig']->addFunction('paginaUrl', new Twig_SimpleFunction('paginaUrl', 'paginaUrl'));
$app['twig']->addFunction('noticiaUrl', new Twig_SimpleFunction('noticiaUrl','noticiaUrl'));
$app['twig']->addFunction('galeriaUrl', new Twig_SimpleFunction('galeriaUrl','galeriaUrl'));

$app['client'] = $app->share(function() use ($app) {
    return new \Secom\Central3\Client('teste');
});
