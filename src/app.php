<?php
include('../bootstrap.php');

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException,
    Secom\Central3\Client\Exception\ApiException,
    Secom\Central3\Client\Exception\CommunicationException,
    Secom\Central3\Client\Helper,
    \Exception;

/* Itens comuns */
$app->before(function() use ($app) {
    $site = $app['client']->query('site.info')[0];
    $title = array($site->nome);
    $app['twig']->addGlobal('title', $title);
    $app['twig']->addGlobal('menu', $app['client']->query('pagina.mapa'));
});

/* Home */
$app->get('/', function (Silex\Application $app) {
    $banners = $app['client']->query('banner.listar');
    $banners = filterBanners($banners,75407);

    $destaques = $app['client']->query('noticia.listar','destaque=s&temfoto=s&thumb=s&limite=1');
    $ids = $destaques->getHead()->ids;
    $noticias = $app['client']->query('noticia.listar',"thumb=s&limite=5&negar={$ids}");
    return $app['twig']->render('index.twig', compact('banners','destaques', 'noticias'));
})->bind('index');

/* Listar notícias */
$app->get('noticias', function (Silex\Application $app){
    try {
        $pagina = $app['request']->get('pagina');
        appendTitle($app, 'Notícias');
        if (!is_numeric($pagina)) { $pagina = 1; }
        $noticias = $app['client']->query('noticia.listar',"pagina={$pagina}&limite=10&thumb=s");
        $pagina++;
        $proximaPagina = ($pagina < $noticias->getHead()->paginas)? $pagina : 0;
        return $app['twig']->render('noticias.twig', compact('noticias', 'proximaPagina'));
    } catch(ApiException $e) {
        throw new NotFoundHttpException($e->getMessage(), $e, 404);
    }
})->bind('noticias');

/* Visualizar notícia */
$app->get('/noticias/{slug}-{id}', function (Silex\Application $app, $id) {
    try {
        $pagina = $app['client']->query('noticia.exibir',"id={$id}");
        if ($app['request']->getPathInfo() != noticiaUrl($pagina)) {
            return $app->redirect(noticiaUrl($pagina));
        }
        appendTitle($app, 'Notícias');
        appendTitle($app, $pagina->titulo);
        return $app['twig']->render('noticia.twig', compact('pagina'));
    } catch(ApiException $e) {
        throw new NotFoundHttpException($e->getMessage(), $e, 404);
    }
})->assert('slug','[a-z0-9\-]+')->bind('noticia');

/* Listar galerias */
$app->get('galerias', function (Silex\Application $app) {
    try {
        $galerias = $app['client']->query('galeria.listar',"thumb=s");
        appendTitle($app, 'Galerias');
        return $app['twig']->render('galerias.twig', compact('galerias'));
    } catch(ApiException $e) {
        throw new NotFoundHttpException($e->getMessage(), $e, 404);
    }
})->bind('galerias');

/* Visualizar galeria */
$app->get('galerias/{slug}-{id}', function (Silex\Application $app, $id) {
    try {
        $galeria = $app['client']->query('galeria.exibir',"id=${id}");
        if ($app['request']->getPathInfo() != galeriaUrl($galeria)) {
            return $app->redirect(galeriaUrl($galeria));
        }
        appendTitle($app, 'Galerias');
        appendTitle($app, $galeria->titulo);
        return $app['twig']->render('galeria.twig', compact('galeria'));
    } catch(ApiException $e) {
        throw new NotFoundHttpException($e->getMessage(), $e, 404);
    }
})->assert('slug','[a-z0-9\-]+')->bind('galeria');

/* Visualizar página */
$app->get('{slug}-{id}', function (Silex\Application $app, $slug, $id) {
    try {
        $pagina = $app['client']->query('pagina.exibir',"id={$id}");
        if ($app['request']->getPathInfo() != paginaUrl($pagina)) {
            return $app->redirect(paginaUrl($pagina));
        }
        appendTitle($app, $pagina->titulo);
        return $app['twig']->render('pagina.twig', compact('pagina'));
    } catch(ApiException $e) {
        throw new NotFoundHttpException($e->getMessage(), $e, 404);
    }
})->assert('slug','[a-z0-9\-]+')->bind('pagina');

/* Tratando erros */
$app->error(function (\Exception $exception) use ($app) {
    if ($exception instanceof NotFoundHttpException) {
        if (isset($app['twig']->getGlobals()['menu'])) {
            return $app['twig']->render('error/error.twig', compact('exception'));
        }
    }
    return $app['twig']->render('error/panic.twig', compact('exception'));
});

return $app;
