# Central 3 Site

clone:
	git clone git@bitbucket.org:secom-tocantins/central3-site.git

cd:
  cd central3-site

run:
  docker-compose up -d

install deps:
  ./console.sh composer install

navigate: http://localhost:3003
