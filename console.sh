#!/bin/bash

function bash
{
    docker-compose exec web bash
}

function logs
{
    docker-compose logs -f web
}

function composer
{
    docker-compose exec web composer "$@"
}

"$@"
