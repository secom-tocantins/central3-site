FROM ubuntu:16.04

MAINTAINER Luan Almeida <luanlmd@gmail.com>

RUN apt update && apt upgrade -y && apt install -y \
 	ca-certificates curl nano git zip -y \
	nginx \
	php php-fpm php-mbstring php-curl php-zip

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

RUN curl -L https://getcomposer.org/composer.phar -o /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer

CMD /etc/init.d/php7.0-fpm start && nginx -g "daemon off;"
